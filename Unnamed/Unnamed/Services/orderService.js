﻿(function () {
    'use strict';
    angular.module('app').factory('OrderService', Service);

    function Service($http, AppSettings) {

        this.getUserOrders = function () {
            var accestoken = sessionStorage.getItem("token");
            var authHeaders = {};
            if (accestoken) {
                authHeaders.Authorization = 'Bearer ' + accestoken;
            }
            var response = $http({
                url: AppSettings.serverPath + "/api/OrdersGetByUser",
                method: "GET",
                headers: authHeaders
            });
            return response;

        };

        this.addNew = function (data) {
            var accestoken = sessionStorage.getItem("token");
            var authHeaders = {};
            if (accestoken) {
                authHeaders.Authorization = 'Bearer ' + accestoken;
            }
            var response = $http({
                url: AppSettings.serverPath + "/api/Orders",
                method: "POST",
                headers: authHeaders,
                data: data
            });
            return response;
        };



        return {
            addNew: this.addNew,
            getUserOrders: this.getUserOrders,
        };
    }

})();