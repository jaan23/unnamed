﻿(function () {
    'use strict';
    angular.module('app').factory('CartService', Service);

    function Service($http, $rootScope, AppSettings) {

        var dItems = [];
        var shoppingCart = {
            items: [],
            amount: 0
        };

        this.getItems = function () {
            shoppingCart.items = sessionStorage.getItem("cartItems") !== null ? JSON.parse(sessionStorage.getItem("cartItems")) : dItems;
            shoppingCart.amount = shoppingCart.items.length;
            return shoppingCart;
        };

        this.addItem = function (cdKey) {
            shoppingCart.items = sessionStorage.getItem("cartItems") !== null ? JSON.parse(sessionStorage.getItem("cartItems")) : dItems;
            shoppingCart.items.push(cdKey);
            shoppingCart.amount = shoppingCart.items.length;
            sessionStorage.setItem("cartItems", JSON.stringify(shoppingCart.items));
        };


        this.deleteItem = function (id) {
            shoppingCart.items = shoppingCart.items.filter(item => {
                return item.gameCdKeyId !== id;
            });
            sessionStorage.setItem("cartItems", JSON.stringify(shoppingCart.items));
        };

        this.deleteAllItems = function () {
            shoppingCart.items = [];
            sessionStorage.setItem("cartItems", JSON.stringify(shoppingCart.items));
        };


        return {
            getItems: this.getItems,
            addItem: this.addItem,
            countItems: this.countItems,
            deleteItem: this.deleteItem,
            deleteAllItems: this.deleteAllItems

        };
    }

})();