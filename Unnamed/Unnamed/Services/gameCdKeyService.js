﻿(function () {
    'use strict';
    angular.module('app').factory('GameCdKeyService', Service);

    function Service($http, AppSettings) {

        this.getAll = function () {
            var response = $http.get(AppSettings.serverPath + '/api/GameCdKeys');
            return response;
        };


        this.getUserCdKeys = function () {
            var accestoken = sessionStorage.getItem("token");
            var authHeaders = {};
            if (accestoken) {
                authHeaders.Authorization = 'Bearer ' + accestoken;
            }
            var response = $http({
                url: AppSettings.serverPath + "/api/UserCdKeys",
                method: "GET",
                headers: authHeaders
            });
            return response;

        };

        this.addNew = function (data) {
            var accestoken = sessionStorage.getItem("token");
            var authHeaders = {};
            if (accestoken) {
                authHeaders.Authorization = 'Bearer ' + accestoken;
            }
            var response = $http({
                url: AppSettings.serverPath + "/api/GameCdKeys",
                method: "POST",
                headers: authHeaders,
                data: data
            });
            return response;
        };

    
        this.getById = function (id) {
            var response = $http.get(AppSettings.serverPath + '/api/GameCdKeys/' + id);
            return response;
        };

        this.getByGameId = function(id) {
            var response = $http.get(AppSettings.serverPath + '/api/GameCdKeys/GetByGameId/' + id);
            return response
        };

        this.remove = function (id) {
            var accestoken = sessionStorage.getItem("token");
            var authHeaders = {};
            if (accestoken) {
                authHeaders.Authorization = 'Bearer ' + accestoken;
            }
            var response = $http({
                url: AppSettings.serverPath + "/api/GameCdKeys/"+id,
                method: "DELETE",
                headers: authHeaders
            });
            return response;
        };

        return {
            getAll: this.getAll,
            getById: this.getById,
            getByGameId: this.getByGameId,
            getAllByGenre: this.getAllByGenre,
            addNew: this.addNew,
            getUserCdKeys: this.getUserCdKeys,
            remove: this.remove
           
        };
    }

})();