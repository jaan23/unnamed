﻿(function () {
    'use strict';

    angular.module('app').factory('RegisterService', Service);

    function Service($http, $q, AppSettings) {
        return function (email, password, confirmPassword) {
            var result = $q.defer();

            $http({
                method: 'POST',
                url: AppSettings.serverPath+'/api/Account/register',
                data: { Email: email, Password: password, ConfirmPassword: confirmPassword },
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                result.resolve(response);
            })
                .catch(function (response) {
                    result.reject(response);
                });


            return result.promise;
        };

    }
})();