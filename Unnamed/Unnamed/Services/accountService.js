﻿(function () {
    'use strict';
    angular.module('app').factory('AccountService', Service);

    function Service($http, $rootScope, AppSettings) {

        var dItems = [];
        var account = {
            token: undefined,
            isLoggedIn: false
        };

        this.getAccount = function () {
            if (sessionStorage.getItem("token") !== null) {
                account.token = sessionStorage.getItem("token");
                account.isLoggedIn = true;
            }
            return account;
        };

        this.setToken = function (token) {
            sessionStorage.setItem("token", token);
            account.token = token;
            account.isLoggedIn = true;
        };


        return {
            getAccount: this.getAccount,
            setToken: this.setToken

        };
    }

})();