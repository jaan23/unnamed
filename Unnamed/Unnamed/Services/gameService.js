﻿(function() {
    'use strict';
    angular.module('app').factory('GameService', Service);

    function Service($http, AppSettings) {
        this.getAll = function() {
            var response = $http.get(AppSettings.serverPath + '/api/games');
            return response;
        };

        this.getAllByGenre = function(genre) {
            var response = $http.get(AppSettings.serverPath + '/api/GetGamesByGenre/' + genre);
            return response;
        };

        this.getById = function(id) {
            var response = $http.get(AppSettings.serverPath + '/api/games/' + id);
            return response;
        };

        this.addNewGame = function(data) {
            var accestoken = sessionStorage.getItem("token");
            var authHeaders = {};
            if (accestoken) {
                authHeaders.Authorization = 'Bearer ' + accestoken;
            }
            var response = $http({
                url: AppSettings.serverPath + "/api/Games",
                method: "POST",
                headers: authHeaders,
                data: data
            });
            return response;
        }

        this.addFavouriteGame = function(data) {
            var accestoken = sessionStorage.getItem("token");
            var authHeaders = {};
            if (accestoken) {
                authHeaders.Authorization = 'Bearer ' + accestoken;
            }
            var response = $http({
                url: AppSettings.serverPath + "/api/Games",
                method: "POST",
                headers: authHeaders,
                data: data
            });
            return response;
        }

        return {
            getAll: this.getAll,
            getById: this.getById,
            getAllByGenre: this.getAllByGenre,
            addNewGame: this.addNewGame,
            addFavouriteGame: this.addFavouriteGame
        };
    }

})();