﻿(function () {
    'use strict';
    angular.module('app').factory('FavouriteService', Service);

    function Service($http, AppSettings) {

        this.getAll = function () {
            var response = $http.get(AppSettings.serverPath + '/api/Favourites');
            return response;
        };

        this.addFavourite = function (data) {
            var accestoken = sessionStorage.getItem("token");
            var authHeaders = {};
            if (accestoken) {
                authHeaders.Authorization = 'Bearer ' + accestoken;
            }
            var response = $http({
                url: AppSettings.serverPath + "/api/Favourites",
                method: "POST",
                headers: authHeaders,
                data: data
            });
            return response;
        }

        return {
            getAll: this.getAll,
            addFavourite: this.addFavourite
        };
    }
})();