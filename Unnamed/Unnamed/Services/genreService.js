﻿(function () {
    'use strict';
    angular.module('app').factory('GenreService', Service);

    function Service($http, AppSettings) {

        this.getAll = function () {
            var response = $http.get(AppSettings.serverPath + '/api/genres');
            return response;
        };

        return {
            getAll: this.getAll
        };
    }

})();