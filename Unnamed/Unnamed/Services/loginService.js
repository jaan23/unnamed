﻿(function () {
    'use strict';
    angular.module('app').factory('LoginService', Service);

    function Service($http, $q, AppSettings) {

        this.login = function (username, password) {
            var params = { grant_type: "password", userName: username, password: password };
            var response = $http({
                method: 'POST',
                url: AppSettings.serverPath + '/token',
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: params,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;' }
            });
            return response;
        };

        this.logout = function () {
            sessionStorage.removeItem("token");
        };

        return {
            login: this.login,
            logout: this.logout
        };
    }


})();