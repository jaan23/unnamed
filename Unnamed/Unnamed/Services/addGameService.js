﻿(function() {
    'use strict';

    angular.module('app').factory('AddGameService', Service);

    function Service($http, AppSettings) {

        this.addNew = function (data) {
            var accestoken = sessionStorage.getItem("token");
            var authHeaders = {};
            if (accestoken) {
                authHeaders.Authorization = 'Bearer ' + accestoken;
            }
            var response = $http({
                url: AppSettings.serverPath + "/api/account/addGame",
                method: "POST",
                headers: authHeaders,
                data: this.data
            });
            return response;
        };

        return {
            addNew: this.addNew
        };
    }
})();