﻿(function() {
    'use strict';

    var app = angular.module('app', ['ngRoute']);
    
    app.directive('search',
        function() {
            return {
                restrict: 'E',
                templateUrl: 'Views/search.html'
            };
        });
})();