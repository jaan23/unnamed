﻿(function () {
    'use strict';

    angular.module('app').controller('SellingController', Ctrl);

    function Ctrl($http, $routeParams, $location, GameService, GameCdKeyService) {
        var vm = this;

        vm.barVisible = false;
        vm.error = undefined;
        vm.games = [];
        vm.cdKeys = [];
        vm.selectedGame = {};
        vm.gameCdKey = {
            cdKey: undefined,
            gameId: undefined,
            price: undefined       
        };

        vm.addCdKey = addCdKey;
        vm.removeCdKey = removeCdKey;
        vm.btnCancel = btnCancel;

        init();
      
        function init() {
            GameService.getAll($routeParams.id).then(function (result) {
                vm.games = result.data;
            }).catch(function (response) {
               
            });        
            initKeyList();
        }


        function initKeyList() {
            GameCdKeyService.getUserCdKeys().then(function (result) {
                vm.cdKeys = result.data;
            }).catch(function (response) {

            });
        }

        function addCdKey() {
            vm.gameCdKey.gameId = vm.selectedGame.gameId;
            vm.test = vm.gameCdKey;
            GameCdKeyService.addNew(vm.gameCdKey).then(function (result) {
                vm.barVisible = false;
                initKeyList();
                clearFields();
            }).catch(error => {
                vm.error = error.data;
            });
        }

        function removeCdKey(id) {
            GameCdKeyService.remove(id).then(() => initKeyList())
                .catch();
        }

        function clearFields() {
            vm.selectedGame = {};
            vm.gameCdKey = {
                cdKey: undefined,
                gameId: undefined,
                price: undefined
            };
            vm.error = undefined;
        }

        function btnCancel() {
            clearFields();
            vm.barVisible = false;
        }

        function buy() {

        }
    }
})();