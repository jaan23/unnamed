﻿(function () {
    'use strict';

    angular.module('app').controller('GameController', Ctrl);

    function Ctrl($http, $rootScope, $routeParams, $location, GameService, GameCdKeyService, CartService) {
        var vm = this;
        vm.game = {};
        vm.cdKeys = [];
        vm.addFavourite = addFavourite;

        vm.addForm = {
            name: undefined,
            year: undefined,
            genre: undefined,
            image: undefined
        };
        vm.addNewGame = addNewGame;
        vm.addFavouriteGame = addFavouriteGame;
        vm.addToShoppingCart = addToShoppingCart;
        vm.test = 1;
        init();

        function init() {
            GameService.getById($routeParams.id).then(function (result) {
                vm.game = result.data;
            });
            initCdKeys($routeParams.id);
        }

        function addFavourite(gameId) {
            GameService.addFavouriteGame(gameId).then(function (result) {
                vm.confirmation = "Success";
            });
        }

        function initCdKeys(gameId) {
            GameCdKeyService.getByGameId(gameId).then(function (result) {
                vm.cdKeys = result.data;
            });
        }

        function addToShoppingCart(cdKey) {
            CartService.addItem(cdKey);
            $rootScope.itemsInShoppingCart = 156;
        }

        $('#game-image').change(function (event) {
            $("img").prop('src', this.value);
        });
    }

})();