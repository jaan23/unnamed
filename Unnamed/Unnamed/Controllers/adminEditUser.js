﻿(function() {
    'use strict';
    angular.module('app').controller('AdminEditUser', AdminEditUserController);

    function AdminEditUserController($http, $location) {
        var vm = this;
        $http.changeView = function(view) {
            $location.path(view);
        };
    }
})();