﻿(function () {
    'use strict';

    angular.module('app').controller('FavouriteController', Ctrl);

    function Ctrl($http, $routeParams, $location, FavouriteService, AccountService) {
        var vm = this;
        init();
        vm.favourites = {};
        //vm.addFavourite = addFavourite;
        vm.account = AccountService.getAccount();
        vm.getAll = getAll();

        function init() {
            getAll();
        }

        function getAll() {
            FavouriteService.getAll(vm.account).then(function(result) {
                vm.favourites = result.data;
            });
        }
    }

})();