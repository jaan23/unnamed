﻿(function () {
    'use strict';

    angular.module('app').controller('MenuController', Ctrl);

    function Ctrl($http, $routeParams, $location, GenreService, LoginService, CartService, AccountService) {
        var vm = this;
        vm.genres = [];
        vm.logout = logout;
        vm.shoppingCart = CartService.getItems();
        vm.account = AccountService.getAccount();
        init();

        function init() {
            GenreService.getAll().then(response => vm.genres = response.data)
                .catch(error => { throw error; });
        }

        function logout() {
            account.isLoggedIn = false;
            LoginService.logout();
        }
       
    }

})();