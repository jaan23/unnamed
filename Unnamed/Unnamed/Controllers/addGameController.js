﻿(function () {
    'use strict';

    angular.module('app').controller('AddGameController', Ctrl);

    function Ctrl($http, $routeParams, $location, GameService, GenreService) {
        var vm = this;
        vm.game = {};
        vm.genres = [];
        vm.selectedGenre = [];
        initGenres();

        vm.addMin = {
            os: undefined,
            processor: undefined,
            memory: undefined,
            graphics: undefined,
            directx: undefined,
            network: undefined,
            storage: undefined,
            additionalNotes: undefined,
            systemRequirementType: undefined
        };

        vm.addForm = {
            name: undefined,
            year: undefined,
            frontCoverUrl: undefined
            //gameGenres: vm.selectedGenre,
            //systemRequirements: vm.addMin
        };
        vm.selectedGenre = vm.addForm.gameGenres;
        vm.addNewGame = addNewGame;

        function initGenres() {
            GenreService.getAll().then(function (result) {
                vm.genres = result.data;
            }).catch(function (response) {
                vm.error = "Error";
            });
        }

        function addNewGame() {
            GameService.addNewGame(vm.addForm).then(function (result) {
                vm.confirmation = "Success";
            });
        }

        $('[name="game-image"]').on('change',
            function (event) {
                $('img.preview').prop('src', this.value);
            });
    }
})();