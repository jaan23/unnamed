﻿(function () {
    'use strict';

    angular.module('app').controller('OrderHistoryController', Ctrl);

    function Ctrl($http, $routeParams, $location, OrderService, FeedbackService) {

        var vm = this;
        
        vm.orders = [];
        vm.numbers = [0, 1, 2, 3, 4, 5];
        vm.selectedNumber = undefined;
        vm.comment = undefined;
        vm.showItems = false;
        vm.showFeedbackField = false;
        vm.selectedItem = undefined;
        vm.selectedOrder = undefined;

        vm.btnClick = btnClick;
        vm.btnExit = btnExit;
        vm.btnGiveFeedback = btnGiveFeedback;
        vm.btnCancel = btnCancel;
        vm.btnAdd = btnAdd;

        vm.feedback = {
            comment: undefined,
            rating: undefined,
            sellerId: undefined,
            gameCdKeyInOrderId: undefined
        
        }
        init();

        function init() {
            OrderService.getUserOrders().then(function (result) {
                vm.orders = result.data;
            })
        }


        function addFeedback(item) {
            vm.feedback.comment = vm.comment;
            vm.feedback.rating = vm.selectedNumber;
            vm.feedback.gameCdKeyInOrderId = item.gameCdKey.gameCdKeyId
            FeedbackService.addNew(vm.feedback).then(function () {
                clearItem();
            }).catch(function () {
            })
        }

        
        function btnClick(order) {
            vm.showItems = true;
            vm.selectedOrder = order;
        }
        function btnExit() {
            clearOrder();
        }
        function btnGiveFeedback(item) {
            vm.showFeedbackField = true;
            vm.selectedItem = item;
        }
        function btnCancel() {
            clearItem();
        }
        function btnAdd(item) {
            addFeedback(item);
        }

        function clearItem() {
            vm.showFeedbackField = false;
            vm.selectedItem = undefined;
            vm.comment = undefined;
            vm.selectedNumber = undefined;
        }

        function clearOrder() {
            vm.selectedOrder = undefined;
            vm.showItems = false;
            clearItem();
        }
    }
})();