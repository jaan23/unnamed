﻿(function() {
    'use strict';
    angular.module('app').controller('HomeController', HomeController);

    function HomeController($location, $http, $routeParams, GameService, FavouriteService) {
        var vm = this;
        vm.games = [];
        init();
        vm.addFavourite = {
            gameId: undefined,
            userId: undefined
        };
        vm.addFavourite = addFavourite;
        function init() {
            if ($routeParams.genre !== undefined) {
                GameService.getAllByGenre($routeParams.genre).then(response => vm.games = response.data)
                    .catch(error => { throw error; });
            } else {
                GameService.getAll().then(response => vm.games = response.data)
                    .catch(error => { throw error; });
            }
        }

        function addFavourite(id) {
            vm.addFavourite.gameId = id;
            FavouriteService.addFavourite(vm.addFavourite.gameId).then(function (result) {
                vm.confirmation = "Success";
            });
        }
    }
})();