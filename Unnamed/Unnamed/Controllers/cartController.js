﻿(function() {
    'use strict';

    angular.module('app').controller('CartController', Ctrl);

    function Ctrl($http, $routeParams, $location, CartService, OrderService) {

        var vm = this;
        vm.shoppingCart = CartService.getItems();
        vm.sum = undefined;
        vm.removeFromCart = removeFromCart;
        vm.buy = buy;
        vm.order = {
            gameCdKeys: []
        }

        init();

        function init() {
            vm.sum = getSum(vm.shoppingCart.price);
        }
        function getSum() {
            var number = 0;
            for (var i = 0; i < vm.shoppingCart.items.length; i++) {
                number += vm.shoppingCart.items[i].price;
            }
            return number;
        }

        function removeFromCart(id) {
            CartService.deleteItem(id);
            init();
        }

        function buy() {
          
           vm.order.gameCdKeys = vm.shoppingCart.items;
           OrderService.addNew(vm.order).then(function () {
               CartService.deleteAllItems();
           }).catch(function () {
            })
        }
    }
})();