﻿(function() {
    'use strict';

    angular.module('app').controller('RegisterController', RegisterController);

    function RegisterController($location, RegisterService) {
        var vm = this;
        vm.registerForm = {
            username: undefined,
            password: undefined,
            confirmPassword: undefined,
            errorPassword: undefined,
            errorConfirmPassword: undefined,
            errorEmail: undefined

        };

        vm.register = function() {
            RegisterService(vm.registerForm.username, vm.registerForm.password, vm.registerForm.confirmPassword)
                .then(function(response) {
                    $location.path('/login');
                })
                .catch(function(response) {
                    vm.registerForm.errorPassword = undefined;
                    vm.registerForm.errorConfirmPassword = response.data.modelState["model.ConfirmPassword"][0];
                    vm.registerForm.errorEmail = response.data.modelState["model.Email"][0];
                });
        };
    }
})();