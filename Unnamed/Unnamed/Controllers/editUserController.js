﻿(function() {
    'use strict';
    angular.module('app').controller('EditController', EditController);

    function EditController($location, EditService) {
        var vm = this;
        vm.editForm = {
            oldPassword: undefined,
            username: undefined,
            password: undefined,
            confirmPassword: undefined,
            errorPassword: undefined,
            errorConfirmPassword: undefined,
            errorEmail: undefined
        };

//        function init() {
//                UserService.getById($routeParams.id).then(function(result){
//                    username = result.email;
//                });
//            };
        
        vm.edit = function () {
            EditService(vm.editForm.oldPassword, vm.editForm.username, vm.editForm.password, vm.editForm.confirmPassword)
                .then(function (response) {
                    $location.path('/home');
                })
                .catch(function (response) {
                    vm.editForm.errorPassword = undefined;
                    vm.editForm.errorConfirmPassword = response.data.modelState["model.ConfirmPassword"][0];
                    vm.editForm.errorEmail = response.data.modelState["model.Email"][0];
                });
        };
    };
})();