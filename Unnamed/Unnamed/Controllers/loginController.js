﻿(function() {
    'use strict';
    angular.module('app').controller('LoginController', LoginController);

    function LoginController($location, LoginService, AccountService) {
        var vm = this;
        vm.loginForm = {
            username: undefined,
            password: undefined,
            errorMessage: undefined
        };

        vm.login = function () {
            LoginService.login(vm.loginForm.username, vm.loginForm.password)
                .then(function (response) {
                    AccountService.setToken(response.data.access_token);      
                }).then(function () {
                    $location.path('/home');
                })
                .catch(function(response) {
                    vm.loginForm.errorMessage = response.data.error_description;
                });
        };
    }
})();