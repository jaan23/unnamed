﻿(function() {
    'use strict';

    angular.module('app').config(RouteConfig);

    function RouteConfig($routeProvider) {

        $routeProvider.when('/login',
            {
                templateUrl: 'Views/login.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            }).when('/register',
            {
                templateUrl: 'Views/register.html',
                controller: 'RegisterController',
                controllerAs: 'vm'
            }).when('/home',
            {
                templateUrl: 'Views/home.html',
                controller: 'HomeController',
                controllerAs: 'vm'
            }).when('/home/:genre',
            {
                templateUrl: 'Views/home.html',
                controller: 'HomeController',
                controllerAs: 'vm'
            }).when('/editUser',
            {
                templateUrl: 'Views/editUser.html',
                controller: 'EditController',
                controllerAs: 'vm'
            }).when('/addGame',
            {
                templateUrl: 'Views/addGame.html',
                controller: 'AddGameController',
                controllerAs: 'vm'
            }).when('/adminEditUser',
            {
                templateUrl: 'Views/adminEditUser.html',
                controller: 'AdminEditUser',
                contreollerAs: 'vm'
            }).when('/game/:id',
            {
                templateUrl: 'Views/game.html',
                controller: 'GameController',
                controllerAs: 'vm'
            }).when('/selling',
            {
                templateUrl: 'Views/selling.html',
                controller: 'SellingController',
                controllerAs: 'vm'
            }).when('/favourite',
            {
                templateUrl: 'Views/favourite.html',
                controller: 'FavouriteController',
                controllerAs: 'vm'
            }).when('/cart',
            {
                templateUrl: 'Views/cart.html',
                controller: 'CartController',
                controllerAs: 'vm'
            }).when('/orderHistory',
            {
                templateUrl: 'Views/orderHistory.html',
                controller: 'OrderHistoryController',
                controllerAs: 'vm'
            }).otherwise('/home');
    }

})();