﻿using BLL.DTO;
using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Factories
{
    public class GameCdKeysInOrderFactory
    {
        GameCdKeyFactory _gameCdKeyFactory;

        public GameCdKeysInOrderFactory()
        {
            _gameCdKeyFactory = new GameCdKeyFactory();
        }
        public GameCdKeysInOrderDTO CreateGameCdKeyInOrderDTO(GameCdKeyInOrder GCKO)
       
        {
            return new GameCdKeysInOrderDTO()
            {
                GameCdKeysInOrderId = GCKO.GameCDKeyId,
                GameCdKeyId = GCKO.GameCdKeyInOrderId,
                GameCdKey = _gameCdKeyFactory.CreateGameCdKeyDTO(GCKO.GameCDKey)
            };
        }

        public GameCdKeyInOrder CreateGameCdKeyInOrder(GameCdKeysInOrderDTO dto)
        {
            return new GameCdKeyInOrder()
            {
                GameCDKeyId = dto.GameCdKeyId,
            };
        }
    }
}
