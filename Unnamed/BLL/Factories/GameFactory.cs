﻿using BLL.DTO;
using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Factories
{
    public class GameFactory
    {
        private readonly SystemRequirementsFactory _systemRequirementsFactory;

        public GameFactory()
        {
            _systemRequirementsFactory = new SystemRequirementsFactory();
        }
        public GameDTO CreateGameDTOForList(Game game)
        {
            return new GameDTO()
            {
                GameId = game.GameId,
                Name = game.Name,
                Year = game.Year,
                FrontCoverUrl = game.FrontCoverUrl
            };
        }
        public GameDTO CreateGameDTO(Game game)
        {
            return new GameDTO()
            {
                GameId = game.GameId,
                Name = game.Name,
                Year = game.Year,
                GameGenres = game.GameGenres.Select(x => new GenreDTO { Id = x.Genre.GenreId, Name = x.Genre.Name }).ToList(),
                FrontCoverUrl = game.FrontCoverUrl,
                //Pictures = game.Illustrations.Select(x => new PicturesDTO { IllustrationID = x.IllustrationId, Url = x.IllustrationLink }).ToList(),
                SystemRequirements = game.SystemRequirements.Select(x => _systemRequirementsFactory.Create(x)).ToList()

            };
        }

        public Game CreateGameByDTO(GameDTO gameDto)
        {
            return new Game()
            {
                GameId = gameDto.GameId,
                Name = gameDto.Name,
                Year = gameDto.Year,
                FrontCoverUrl = gameDto.FrontCoverUrl
            };
        }
    }
}

