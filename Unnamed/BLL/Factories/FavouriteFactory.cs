﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using Domain.Tables;

namespace BLL.Factories
{
    public class FavouriteFactory
    {
        public FavouriteDTO CreateFavouriteDTOForList(Favourite favourite)
        {
            return new FavouriteDTO()
            {
                FavouriteId = favourite.FavouriteId,
                GameId = favourite.GameId,
                UserId = favourite.UserId
            };
        }
        public Favourite CreateFavouriteByDTO(FavouriteDTO favouriteDto, int userId)
        {
            return new Favourite()
            {
                FavouriteId = favouriteDto.FavouriteId,
                GameId = userId,
                UserId = favouriteDto.UserId
            };
        }
    }
}