﻿using BLL.DTO;
using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Factories
{
    public class FeedbackFactory
    {
        public FeedbackDTO CreateFeedbackDTO(Feedback feedback)
        {
            return new FeedbackDTO()
            {
                FeedbackId = feedback.FeedbackId,
                BuyerId = feedback.BuyerId,
                Comment = feedback.Comment,
                GameCdKeyInOrderId = feedback.GameCdKeyInOrderId,
                Rating = feedback.Rating
                
            };
        }

        public Feedback CreateFeedback(FeedbackDTO dto)
        {
            return new Feedback()
            {
                FeedbackId = dto.FeedbackId,
                BuyerId = dto.BuyerId,
                GameCdKeyInOrderId = dto.GameCdKeyInOrderId,
                Comment = dto.Comment,
                Rating = dto.Rating
            };
        }
    }
}
