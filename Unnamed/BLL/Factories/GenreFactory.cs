﻿using BLL.DTO;
using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Factories
{
    public class GenreFactory
    {
        public GenreDTO Create(Genre genre)
        {
            return new GenreDTO()
            {
                Id = genre.GenreId,
                Name = genre.Name,
            };


        }

    }
}
