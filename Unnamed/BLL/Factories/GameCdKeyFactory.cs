﻿using BLL.DTO;
using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Factories
{
    public class GameCdKeyFactory
    {
        public GameCdKeyDTO CreateGameCdKeyDTO(GameCdKey gameCdKey)
        {
            return new GameCdKeyDTO()
            {
                GameCdKeyId = gameCdKey.GameCdKeyId,
                GameName = gameCdKey.Game.Name,
                Price = gameCdKey.Price,
                SellerName= gameCdKey.Seller.UserName,
                IsActive = gameCdKey.IsActive,
                GameId = gameCdKey.GameId,
                SellerId = gameCdKey.SellerId
            };
        }

        public GameCdKey CreateGameCdKey(GameCdKeyDTO dto)
        {
            return new GameCdKey()
            {
                GameCdKeyId = dto.GameCdKeyId,
                CDKey = dto.CDKey,
                GameId = dto.GameId,
                SellerId = dto.SellerId,
                IsActive = dto.IsActive,
                Price = dto.Price
            };
        }
    }
}
