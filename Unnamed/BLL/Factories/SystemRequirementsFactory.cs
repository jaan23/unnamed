﻿using BLL.DTO;
using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Factories
{
    public class SystemRequirementsFactory
    {
        public SystemRequirementsDTO Create(SystemRequirements sr)
        {
            return new SystemRequirementsDTO()
            {
                Id = sr.GameId,
                SystemRequirementType = sr.SystemRequirementsType.Name,
                Os = sr.Os,
                Processor = sr.Processor,
                Memory = sr.Memory,
                Storage = sr.Storage,
                Graphics = sr.Graphics,
                Network = sr.Network,
                DirectX = sr.DirectX,
                AdditionalNotes = sr.AdditionalNotes
            };
        }

        public SystemRequirements CreateByDTO(SystemRequirementsDTO systemRequirementsDto)
        {
            return new SystemRequirements()
            {
                Os = systemRequirementsDto.Os,
                Processor = systemRequirementsDto.Processor,
                Memory = systemRequirementsDto.Memory,
                Storage = systemRequirementsDto.Storage,
                Graphics = systemRequirementsDto.Graphics,
                Network = systemRequirementsDto.Network,
                DirectX = systemRequirementsDto.DirectX,
                AdditionalNotes = systemRequirementsDto.AdditionalNotes
            };
        }

    }
}

