﻿using BLL.DTO;
using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Factories
{
    public class OrderFactory
    {
        GameCdKeysInOrderFactory _gameCdKeysInOrderFactory;
        GameCdKeyFactory _gameCdKeyFactory;
        public OrderFactory()
        {
            _gameCdKeysInOrderFactory = new GameCdKeysInOrderFactory();
            _gameCdKeyFactory = new GameCdKeyFactory();
        }
        public OrderDTO CreateOrderDTO(Order order)
        {
            return new OrderDTO()
            {
                OrderId = order.OrderId,
                GameCdKeysInOrder = order.GameCdKeyInOrder.Select(x=> _gameCdKeysInOrderFactory.CreateGameCdKeyInOrderDTO(x)).ToList(),
                OrderDate = order.OrderDate,
                BuyerId = order.BuyerId,
            };
        }

        public Order CreateOrder(OrderCdKeysDTO dto)
        {

            return new Order()
            {
                OrderId = dto.OrderId,
                BuyerId = dto.BuyerId,
                GameCdKeyInOrder = dto.GameCdKeys.Select(x => new GameCdKeyInOrder {GameCDKeyId = x.GameCdKeyId}).ToList(),
                OrderDate = dto.OrderDate,
                          
            };
        }
    }
}
