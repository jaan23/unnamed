﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using Domain.Tables;

namespace BLL.Factories
{
    public class GameGenreFactory
    {
        public GameGenreDTO Create(GameGenre gameGenre)
        {
            return new GameGenreDTO()
            {
                GameGenreId = gameGenre.GameGenreId,
                GameId = gameGenre.GameId,
                GenreId = gameGenre.GenreId
            };
        }
    }

}
