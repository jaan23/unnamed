﻿using BLL.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Tables;
using BLL.DTO;
using Interfaces;
using BLL.Factories;

namespace BLL.Service
{
    public class OrderService : IOrderService
    {
        private readonly IUow _uow;
        private readonly OrderFactory _orderFactory;
        private readonly GameCdKeyFactory _gameCdKeyFactory;
        public OrderService(IUow uow)
        {
            _uow = uow;
            _orderFactory = new OrderFactory();
            _gameCdKeyFactory = new GameCdKeyFactory();
        }

        public void AddNew(OrderCdKeysDTO entity)
        {
            var domain = _orderFactory.CreateOrder(entity);
            _uow.Order.Add(domain);
            _uow.Commit();
        }

        public List<OrderDTO> GetByUserId(int userId)
        {
            return _uow.Order.All.Where(x => x.BuyerId == userId).Select(x => _orderFactory.CreateOrderDTO(x)).ToList();
        }
    }
}
