﻿using BLL.Service.Interfaces;
using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Factories;
using Interfaces;

namespace BLL.Service
{
    public class GameCdKeyService : IGameCdKeyService
    {
        private readonly IUow _uow;
        private readonly GameCdKeyFactory _gameCdKeyFactory;

        public GameCdKeyService(IUow uow)
        {
            _uow = uow;
            _gameCdKeyFactory = new GameCdKeyFactory();
        }
        public void AddNew(GameCdKeyDTO entity)
        {
            var domain = _gameCdKeyFactory.CreateGameCdKey(entity);
            _uow.GameCdKey.Add(domain);
            _uow.Commit();
        }

        public List<GameCdKeyDTO> GetByGameId(int gameId)
        {
            return _uow.GameCdKey.All.Where(x => x.GameId == gameId)
                .Select(x => _gameCdKeyFactory.CreateGameCdKeyDTO(x)).ToList();
        }

    
        public bool Delete(int userId, int keyId)
        {
            GameCdKey cdKey = _uow.GameCdKey.GetById(keyId);

            if(cdKey.SellerId == userId)
            {
                _uow.GameCdKey.Delete(keyId);
                _uow.Commit();
                return true;
            }
            else
            {
                return false;
            }
           
        }

        public bool FindByCdKey(string key)
        {
            return _uow.GameCdKey.GetByCdKey(key) != null;
        }

        

        public List<GameCdKeyDTO> GetUserCdKeys(int userId)
        {
            return _uow.GameCdKey.All.Where(x => x.SellerId == userId).Select(x => _gameCdKeyFactory.CreateGameCdKeyDTO(x)).ToList();
        }

   
        public GameCdKeyDTO GetById(int id)
        {
            return _gameCdKeyFactory.CreateGameCdKeyDTO(_uow.GameCdKey.GetById(id));
        }

        public List<GameCdKeyDTO> GetAll()
        {
            return _uow.GameCdKey.All.Select(x => _gameCdKeyFactory.CreateGameCdKeyDTO(x)).ToList();
        }
    }
}
