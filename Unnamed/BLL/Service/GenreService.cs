﻿using BLL.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Tables;
using Interfaces;
using BLL.Factories;
using BLL.DTO;

namespace BLL.Service
{
    public class GenreService : IGenreService
    {
        private readonly IUow _uow;
        private readonly GenreFactory _genreFactory;
        public GenreService(IUow uow)
        {
            _uow = uow;
            _genreFactory = new GenreFactory();
        }

        public void AddNew(GenreDTO entity)
        {
            throw new NotImplementedException();
        }

        public List<GenreDTO> GetAll()
        {
            return _uow.Genre.All.Select(x => _genreFactory.Create(x)).ToList();
        }

        public GenreDTO GetById(int id)
        {
            return _genreFactory.Create(_uow.Genre.GetById(id));
        }

    }
}
