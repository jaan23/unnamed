﻿using BLL.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using Interfaces;
using BLL.Factories;

namespace BLL.Service
{
    public class FeedbackService : IFeedbackService
    {

        private readonly IUow _uow;
        private readonly FeedbackFactory _feedbackFactory;

        public FeedbackService(IUow uow)
        {
            _uow = uow;
            _feedbackFactory = new FeedbackFactory();
        }


        public void AddNew(FeedbackDTO entity)
        {
            var domain = _feedbackFactory.CreateFeedback(entity);
            _uow.Feedback.Add(domain);
            _uow.Commit();
        }

        public List<FeedbackDTO> GetBySellerId(int userId)
        {
            return _uow.Feedback.All.Where(x => x.GameCdKeyInOrder.GameCDKey.SellerId == userId).Select(x => _feedbackFactory.CreateFeedbackDTO(x)).ToList();
        }
    }
}
