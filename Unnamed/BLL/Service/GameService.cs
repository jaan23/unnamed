﻿using BLL.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using Interfaces;
using BLL.Factories;
using Domain.Tables;

namespace BLL.Service
{
    public class GameService : IGameService
    {
        private readonly IUow _uow;
        private readonly GameFactory _gameFactory;
        private readonly IGameRepository _gameRepository;
        private readonly SystemRequirementsFactory _systemRequirementsFactory;

        public GameService(IUow uow)
        {
            _uow = uow;
            _gameFactory = new GameFactory();
            _systemRequirementsFactory = new SystemRequirementsFactory();
        }

        public List<GameDTO> GetAll()
        {
            return _uow.Game.All.Select(x => _gameFactory.CreateGameDTOForList(x)).ToList();
        }

        public GameDTO GetById(int id)
        {
            return _gameFactory.CreateGameDTO(_uow.Game.GetById(id));
        }

        public List<GameDTO> GetByGenre(int id)
        {
            return _uow.Genre.GetById(id).GameGenres.Select(x => _gameFactory.CreateGameDTOForList(x.Game)).ToList();
        }

        public List<GameDTO> GetByGenre(string genre)
        {
            return _uow.GameGenre.All.Where(x => x.Genre.Name.Equals(genre))
                .Select(x => _gameFactory.CreateGameDTOForList(x.Game)).ToList();
        }

        public bool GetByName(string name)
        {
            var games = _uow.Game.All;
            return games.Any(game => game.Name.Equals(name));
        }

        public void AddNew(GameDTO game)
        {
            var domain = _gameFactory.CreateGameByDTO(game);
            _uow.Game.Add(domain);
            _uow.Commit();
        }

        public bool Delete(int gameId)
        {
            Game id = _uow.Game.GetById(gameId);

            _uow.Game.Delete(id);
            _uow.Commit();
            return true;
        }
        public void AddRequirements(SystemRequirementsDTO systemRequirementsDto)
        {
            var domain = _systemRequirementsFactory.CreateByDTO(systemRequirementsDto);
            _uow.SystemRequirements.Add(domain);
            _uow.Commit();
        }
    }
}