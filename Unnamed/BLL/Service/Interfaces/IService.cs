﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Service.Interfaces
{
    public interface IService<T> where T: class
    {
        T GetById(int id);
        List<T> GetAll();
        void AddNew(T entity);
    }
}
