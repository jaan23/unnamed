﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Factories;
using Domain.Tables;

namespace BLL.Service.Interfaces
{
    public interface IGameService: IService<GameDTO>
    {
        List<GameDTO> GetByGenre(string genre);
        bool GetByName(string name);
        bool Delete(int gameId);
        void AddRequirements(SystemRequirementsDTO requirements);

    }
}
