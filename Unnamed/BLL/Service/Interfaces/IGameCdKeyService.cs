﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Service.Interfaces
{
    public interface IGameCdKeyService: IService<GameCdKeyDTO>
    {
        List<GameCdKeyDTO> GetByGameId(int gameId);
        List<GameCdKeyDTO> GetUserCdKeys(int userId);
        bool FindByCdKey(string key);
        bool Delete(int userId, int keyId);
        
    }
}
