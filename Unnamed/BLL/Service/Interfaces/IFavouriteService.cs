﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;

namespace BLL.Service.Interfaces
{
    public interface IFavouriteService
    {
        List<FavouriteDTO> GetAll();
        void AddNew(FavouriteDTO favourite, int userID);
        FavouriteDTO GetById(int id);
        FavouriteDTO GetByIds(int gameId, int userID);
    }
}
