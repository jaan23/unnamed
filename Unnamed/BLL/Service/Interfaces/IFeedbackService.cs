﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Service.Interfaces
{
    public interface IFeedbackService
    {
        List<FeedbackDTO> GetBySellerId(int userId);
        void AddNew(FeedbackDTO entity);
        
    }
}
