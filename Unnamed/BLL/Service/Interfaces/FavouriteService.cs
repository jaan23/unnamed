﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Factories;
using Interfaces;
using BLL.DTO;
using BLL.Service.Interfaces;
using Domain.Tables;

namespace BLL.Service
{
    public class FavouriteService : IFavouriteService
    {
        private readonly FavouriteFactory _favouriteFactory;
        private readonly IUow _uow;

        public FavouriteService(IUow uow)
        {
            _uow = uow;
            _favouriteFactory = new FavouriteFactory();
        }

        public List<FavouriteDTO> GetAll()
        {
            return _uow.Favourite.All.Select(x => _favouriteFactory.CreateFavouriteDTOForList(x)).ToList();
        }

        public FavouriteDTO GetByIds(int gameId, int userID)
        {
            return _favouriteFactory.CreateFavouriteDTOForList(_uow.Favourite.GetById(gameId, userID));
        }

        public void AddNew(FavouriteDTO favouriteDto, int userId)
        {
            var domain = _favouriteFactory.CreateFavouriteByDTO(favouriteDto, userId);
            _uow.Favourite.Add(domain);
            _uow.Commit();
        }

        public FavouriteDTO GetById(int id)
        {
            return _favouriteFactory.CreateFavouriteDTOForList(_uow.Favourite.GetById(id));
        }

        public bool Delete(int favId)
        {
            Favourite id = _uow.Favourite.GetById(favId);
            _uow.Favourite.Delete(id);
            _uow.Commit();
            return true;
        }
    }
}
