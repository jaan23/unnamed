﻿using BLL.DTO;
using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Service.Interfaces
{
    public interface IOrderService
    {
        List<OrderDTO> GetByUserId(int userId);
        void AddNew(OrderCdKeysDTO entity);
    }
}
