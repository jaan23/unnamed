﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class OrderCdKeysDTO
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public int BuyerId { get; set; }
        public List<GameCdKeyDTO> GameCdKeys { get; set; }
    }
}
