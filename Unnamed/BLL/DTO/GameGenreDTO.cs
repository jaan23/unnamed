﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class GameGenreDTO
    {
        public int GameGenreId { get; set; }
        public int GameId { get; set; }
        public int GenreId { get; set; }
    }

}
