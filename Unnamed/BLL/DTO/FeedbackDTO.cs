﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class FeedbackDTO
    {
        public int FeedbackId { get; set; }
        public string Comment { get; set; }
        public int Rating { get; set; }
        public int BuyerId { get; set; }
        public int GameCdKeyInOrderId { get; set; }
    }
}
