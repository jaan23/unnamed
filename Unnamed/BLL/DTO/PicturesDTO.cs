﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class PicturesDTO
    {
        public int IllustrationID { get; set; }
        public string Url { get; set; }
        public string IllustrationType { get; set; }
    }
}
