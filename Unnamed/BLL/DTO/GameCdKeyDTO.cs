﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class GameCdKeyDTO
    {
        public int GameCdKeyId { get; set; }
        public string CDKey { get; set; }
        public string GameName { get; set; }
        public float Price { get; set; }
        public Boolean IsActive { get; set; }
        public int GameId { get; set; }
        public int SellerId { get; set; }
        public string SellerName { get; set; }
    }
}
