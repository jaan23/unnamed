﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class SystemRequirementsDTO
    {
        public int Id { get; set; }
        public string Os { get; set; }
        public string Processor { get; set; }

        public string Memory { get; set; }
        public string Graphics { get; set; }
        public string DirectX { get; set; }
        public string Network { get; set; }
        public string Storage { get; set; }
        public string AdditionalNotes { get; set; }
        public string SystemRequirementType { get; set; }

    }
}
