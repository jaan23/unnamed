﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class FavouriteDTO
    {
        public int FavouriteId { get; set; }
        public int GameId { get; set; }
        public int UserId { get; set; }
    }
}
