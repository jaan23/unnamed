﻿using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class GameDTO
    {
        public int GameId { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public string FrontCoverUrl { get; set; }
        public List<GenreDTO> GameGenres { get; set; }
        //public List<PicturesDTO> Pictures { get; set; }
        public List<SystemRequirementsDTO> SystemRequirements { get; set; }
    }
}
