﻿using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class GameCdKeysInOrderDTO
    {
        public int GameCdKeysInOrderId { get; set; }
        public int GameCdKeyId { get; set; }
        public GameCdKeyDTO GameCdKey { get; set; }
    }
}
