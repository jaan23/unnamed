﻿using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IGameCdKeyRepository: IRepository<GameCdKey>
    {
        GameCdKey GetByCdKey(string key);
    }
}
