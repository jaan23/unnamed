﻿using Domain.Tables;

namespace Interfaces
{
    public interface ISystemRequirements : IRepository<SystemRequirements>
    {
    }
}