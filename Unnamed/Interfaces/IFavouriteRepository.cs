﻿using Domain.Tables;

namespace Interfaces
{
    public interface IFavouriteRepository : IRepository<Favourite>
    {
    }
}