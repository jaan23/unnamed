﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IRepositoryFactories
    {
        Func<IAppDbContext, object> GetStandardRepositoryFactory<TEntity>()
         where TEntity : class;
        Func<IAppDbContext, object> GetCustomRepositoryFactory<TRepoInterface>();
    }
}
