﻿using System.Collections.Generic;
using Domain.Identity;

namespace Interfaces
{
    public interface IUserLoginIntRepository : IUserLoginRepository<UserLoginInt>
    {
    }

    public interface IUserLoginRepository : IUserLoginRepository<UserLogin>
    {
    }

    public interface IUserLoginRepository<TUserLogin> : IRepository<TUserLogin>
        where TUserLogin : class
    {
        List<TUserLogin> GetAllIncludeUser();
        TUserLogin GetUserLoginByProviderAndProviderKey(string loginProvider, string providerKey);
    }
}