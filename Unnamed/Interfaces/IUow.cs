﻿using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IUow
    {
     
        T GetRepository<T>() where T : class;

        IGameCdKeyRepository GameCdKey { get;}
        IGameRepository Game { get; }
        IGameGenreRepository GameGenre { get; }
        IGenreRepository Genre { get; }
        IUserIntRepository UsersInt { get; }
        IUserRoleIntRepository UserRolesInt { get; }
        IRoleIntRepository RolesInt { get; }
        IUserClaimIntRepository UserClaimsInt { get; }
        IUserLoginIntRepository UserLoginsInt { get; }
        IFavouriteRepository Favourite { get; }
        ISystemRequirements SystemRequirements { get; }
        IOrderRepository Order { get; }
        IFeedbackRepository Feedback { get; }
        
        
        void Commit();
    }
}
