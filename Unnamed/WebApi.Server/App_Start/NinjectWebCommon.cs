using DAL;
using DAL.Helpers;
using Domain.Identity;
using Identity;
using Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(WebApi.Server.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(WebApi.Server.App_Start.NinjectWebCommon), "Stop")]

namespace WebApi.Server.App_Start
{
    using BLL.Service;
    using BLL.Service.Interfaces;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using System;
    using System.Web;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IAppDbContext>().To<AppDbContext>().InRequestScope();
            //kernel.Bind<IDbContextFactory>().To<DbContextFactory>().InRequestScope();

            kernel.Bind<RepositoryFactories>().To<RepositoryFactories>().InSingletonScope();
            kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>().InRequestScope();
            kernel.Bind<IUow>().To<Uow>().InRequestScope();
            kernel.Bind<IGameService>().To<GameService>().InRequestScope();
            kernel.Bind<IGenreService>().To<GenreService>().InRequestScope();
            kernel.Bind<IGameCdKeyService>().To<GameCdKeyService>().InRequestScope();
            kernel.Bind<IOrderService>().To<OrderService>().InRequestScope();
            kernel.Bind<IFeedbackService>().To<FeedbackService>().InRequestScope();
            //kernel.Bind<IUserStore<User>>().To<UserStore<>>().InRequestScope();
            //kernel.Bind<IRoleStore<Role>>().To<RoleStore>();
            kernel.Bind<IUserStore<UserInt, int>>().To<UserStoreInt>().InRequestScope();
            kernel.Bind<IRoleStore<RoleInt, int>>().To<RoleStoreInt>().InRequestScope();
            
            kernel.Bind<ApplicationSignInManager>().To<ApplicationSignInManager>().InRequestScope();
            kernel.Bind<ApplicationUserManager>().To<ApplicationUserManager>().InRequestScope();
            kernel.Bind<ApplicationRoleManager>().To<ApplicationRoleManager>().InRequestScope();
           
            kernel.Bind<IAuthenticationManager>().ToMethod(a => HttpContext.Current.GetOwinContext().Authentication).InRequestScope();


            // http://stackoverflow.com/questions/5646820/logger-wrapper-best-practice
            kernel.Bind<NLog.ILogger>().ToMethod(a => NLog.LogManager.GetCurrentClassLogger());

          
        }
    }
}
