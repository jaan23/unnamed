﻿using BLL.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace WebApi.Server.Controllers.Api
{
    public class GenresController : ApiController
    {
        private readonly IGenreService _genreservice;
        
        public GenresController(IGenreService genreservice)
        {
            _genreservice = genreservice;
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_genreservice.GetAll());
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult Get(int id)
        {
            var result = _genreservice.GetById(id);
            if (result == null) return NotFound();
            return Ok(result);
        }
    }
}