﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace WebApi.Server.Controllers.Api
{
    public class GameGenresController : ApiController
    {
        private readonly IUow _uow;
       
        public GameGenresController(IUow uow)
        {
            _uow = uow;
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_uow.GameGenre.All);
        }
    }
}