﻿using BLL.Service.Interfaces;
using DAL;
using DAL.Repositories;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using BLL.DTO;
using BLL.Factories;
using Domain.Tables;

namespace WebApi.Server.Controllers.Api
{
    public class GamesController : ApiController
    {
        private readonly IGameService _gameservice;
        // GET: Games

        public GamesController(IGameService gameservice)
        {
            _gameservice = gameservice;
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_gameservice.GetAll());
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult Get(int Id)
        {
            var result = _gameservice.GetById(Id);
            if (result == null) return NotFound();
            return Ok(result);
        }


        [System.Web.Http.Route("api/GetGamesByGenre/{genre}")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult Get(string genre)
        {
            var result = _gameservice.GetByGenre(genre);
            if (result == null) return NotFound();
            return Ok(result);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Authorize]
        public IHttpActionResult PostGame([FromBody] GameDTO game)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_gameservice.GetByName(game.Name))
            {
                _gameservice.AddNew(game);
                return StatusCode(HttpStatusCode.NoContent);
            }
            return StatusCode(HttpStatusCode.Forbidden);
        }
    }
}