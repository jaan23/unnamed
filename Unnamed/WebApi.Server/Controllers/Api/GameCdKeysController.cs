﻿using BLL.DTO;
using BLL.Service.Interfaces;
using Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;


namespace WebApi.Server.Controllers.Api
{
    public class GameCdKeysController : ApiController
    {
        private readonly IGameCdKeyService _gameCdKeyService;
        // GET: Games

        public GameCdKeysController(IGameCdKeyService gameCdKeyService)
        {
            _gameCdKeyService = gameCdKeyService;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_gameCdKeyService.GetAll());
        }


        [Route("api/UserCdKeys")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult GetUserCdKeys()
        {
            int userId = User.Identity.GetUserId<int>();
            return Ok(_gameCdKeyService.GetUserCdKeys(userId));
          
        }


        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var result = _gameCdKeyService.GetById(id);
            if (result == null) return NotFound();
            return Ok(result);
        }

        [Route("api/GameCdKeys/GetByGameId/{id}")]
        [HttpGet]
        public IHttpActionResult GetByGameId(int id)
        {
            var result = _gameCdKeyService.GetByGameId(id);
            if (result == null) return NotFound();
            return Ok(result);
        }

        
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            int userID = User.Identity.GetUserId<int>();
            var result = _gameCdKeyService.Delete(userID,id);
            return result ? StatusCode(HttpStatusCode.NoContent) : StatusCode(HttpStatusCode.Forbidden);
        }

        [HttpPost]
        [Authorize]
        public IHttpActionResult Post([FromBody] GameCdKeyDTO gameCdKey)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_gameCdKeyService.FindByCdKey(gameCdKey.CDKey))
            {
                int userID = User.Identity.GetUserId<int>();
                gameCdKey.SellerId = userID;
                _gameCdKeyService.AddNew(gameCdKey);
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                return Content(HttpStatusCode.BadRequest, "Cd key already exists");
            }
        }
    }
}