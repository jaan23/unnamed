﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using BLL.DTO;
using BLL.Service.Interfaces;
using Microsoft.AspNet.Identity;

namespace WebApi.Server.Controllers.Api
{
    public class FavouritesController : ApiController
    {

        private readonly IFavouriteService _favouriteService;


        public FavouritesController(IFavouriteService favouriteService)
        {
            _favouriteService = favouriteService;
        }


        [System.Web.Http.HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_favouriteService.GetAll());
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult Get(int Id)
        {
            int userId = User.Identity.GetUserId<int>();
            var result = _favouriteService.GetById(Id);
            if (_favouriteService.GetById(userId).Equals(null)) return NotFound();
            return Ok(result);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Authorize]
        public IHttpActionResult Post([FromBody] FavouriteDTO favourite)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}
            int userID = User.Identity.GetUserId<int>();
            if (_favouriteService.GetByIds(favourite.GameId, userID).Equals(false))
            {
                _favouriteService.AddNew(favourite, userID);
                return StatusCode(HttpStatusCode.NoContent);
            }
            return Content(HttpStatusCode.BadRequest, "Game already favourite");
        }
    }
}