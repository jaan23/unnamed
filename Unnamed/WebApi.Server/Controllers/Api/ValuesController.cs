﻿using Interfaces;
using System;
using System.Web.Http;

namespace WebApi.Server.Controllers.Api
{
    public class ValuesController : ApiController
    {
        private readonly IUow _uow;

        public ValuesController(IUow uow)
        {
            _uow = uow;
        }

        // GET api/values
        public string Get()
        {
            return DateTime.Now + " " + _uow.UsersInt.All.Count;
        }
    }
}
