﻿using System.Net;
using System.Web.Http;
using BLL.DTO;
using System.Web.Mvc;
using BLL.Service.Interfaces;

namespace WebApi.Server.Controllers.Api
{
    public class SystemRequirementsController : ApiController
    {
        private readonly IGameService _gameservice;

        public SystemRequirementsController(IGameService gameservice)
        {
            _gameservice = gameservice;
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Authorize]
        public IHttpActionResult PostRequirements([FromBody] SystemRequirementsDTO requirements)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _gameservice.AddRequirements(requirements);
            return StatusCode(HttpStatusCode.OK);
        }
    }
}