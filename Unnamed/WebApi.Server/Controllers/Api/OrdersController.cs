﻿using BLL.DTO;
using BLL.Service.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace WebApi.Server.Controllers.Api
{
    public class OrdersController : ApiController
    {

        private readonly IOrderService _orderService;

        
        public OrdersController(IOrderService orderService)
        {
            _orderService = orderService;
        }



        [Route("api/OrdersGetByUser")]
        [HttpGet]
        public IHttpActionResult GetByUserId()
        {
            int userId = User.Identity.GetUserId<int>();
            var result = _orderService.GetByUserId(userId);
            if (result == null) return NotFound();
            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public IHttpActionResult Post([FromBody] OrderCdKeysDTO order)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}
            int userID = User.Identity.GetUserId<int>();
                order.BuyerId = userID;
                _orderService.AddNew(order);
                return StatusCode(HttpStatusCode.NoContent);           
        }
    }
}