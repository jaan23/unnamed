﻿using BLL.DTO;
using BLL.Service.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace WebApi.Server.Controllers.Api
{
    public class FeedbackController : ApiController
    {
        private readonly IFeedbackService _feedbackService;
        // GET: Games

        public FeedbackController(IFeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }


        [HttpPost]
        [Authorize]
        public IHttpActionResult Post([FromBody] FeedbackDTO feedback)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            int userID = User.Identity.GetUserId<int>();
            feedback.BuyerId = userID;
            _feedbackService.AddNew(feedback);

            return StatusCode(HttpStatusCode.NoContent);

        }
    }
}
