﻿using Domain.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Tables
{
    public class GameCdKey
    {
        public int GameCdKeyId { get; set; }

        public Boolean IsActive { get; set; }
        [Required]
        [MaxLength(length: 35)]
        public string CDKey { get; set; }
        public float Price { get; set; }

        public int SellerId { get; set; }
        [ForeignKey("SellerId")]
        public virtual UserInt Seller { get; set; }

        public int GameId { get; set; }
        public virtual Game Game { get; set; }

        public virtual List<GameCdKeyInOrder> GameCdKeyInOrder { get; set; }



    }
}
