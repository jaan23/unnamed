﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Domain.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Tables
{
    public class Feedback
    {
        public int FeedbackId { get; set; }

        public string Comment { get; set; }

        public int Rating { get; set; }

        public int BuyerId { get; set; }
        [ForeignKey("BuyerId")]
        public UserInt Buyer { get; set; }

        public int GameCdKeyInOrderId{get;set;}
        [ForeignKey("GameCdKeyInOrderId")]
        public virtual GameCdKeyInOrder GameCdKeyInOrder { get; set; }

    }
}
