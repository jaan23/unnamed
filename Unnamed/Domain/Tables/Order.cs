﻿using Domain.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Tables
{
    public class Order
    {
        public int OrderId { get; set; } 
        public DateTime OrderDate { get; set; }              
        public int BuyerId { get; set; }
        public UserInt Buyer { get; set; }
        public virtual List<GameCdKeyInOrder> GameCdKeyInOrder { get; set; }

    }
}
