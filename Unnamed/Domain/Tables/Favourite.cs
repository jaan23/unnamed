﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Tables
{
    public class Favourite
    {   [Key]
        public int FavouriteId { get; set; }
        public int GameId { get; set; }
        public int UserId { get; set; }
    }
}