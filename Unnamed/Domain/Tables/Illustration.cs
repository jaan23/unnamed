﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Domain.Tables
{
    public class Illustration
    {
        [Key]
        public int IllustrationId { get; set; }

        public string IllustrationLink { get; set; }


        public int IllustrationTypeId { get; set; }
        public IllustrationType IllustrationType { get; set; }


        public int GameId { get; set; }
        public virtual Game Game { get; set; }
    }
}
