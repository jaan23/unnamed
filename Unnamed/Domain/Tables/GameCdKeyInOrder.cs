﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Tables
{
    public class GameCdKeyInOrder
    {
        public int GameCdKeyInOrderId { get; set; }
        public int GameCDKeyId { get; set; }
        public virtual GameCdKey GameCDKey { get; set; }
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
        public virtual List<Feedback> Feedback{get; set;}
    }
}
