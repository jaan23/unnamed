﻿using Domain.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Tables
{ 
   public class Genre
    {
        public int GenreId { get; set; }

        [Required]
        [MaxLength(length: 20)]
        public string Name { get; set; }
        public virtual List<GameGenre> GameGenres { get; set; }
    }
}
