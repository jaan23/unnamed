﻿using Domain.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Tables
{
    public class Game
    {
        [Key]
        public int GameId { get; set; }

        public string Name { get; set; }

        public int Year { get; set; }

        public string FrontCoverUrl { get; set; }

        public virtual List<GameGenre> GameGenres { get; set; }

        public virtual List<SystemRequirements> SystemRequirements { get; set; }

        public virtual List<Illustration> Illustrations { get; set; }

      //  public virtual List<GameCdKey> GameCDKey { get; set; }

       
    }
}
