﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Tables
{
    public class IllustrationType
    {
        public int IllustrationTypeId { get; set; }

        public string Name { get; set; }

        public virtual List<Illustration> Illustrations { get; set; }
    }
}
