﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Domain.Tables
{
    public class SystemRequirementsType
    {
        [Key]
        public int SystemRequirementTypeId{ get; set; }

        [Required]
        [MaxLength(length: 30)]
        public string Name { get; set; }
        public virtual List<SystemRequirements> SystemRequirements { get; set; }

    }
}
