﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Domain.Tables;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Tables
{
    public class SystemRequirements
    {
        public int SystemRequirementsId { get; set; }

        [Required]
        [MaxLength(length: 20)]
        public string Os { get; set; }

        [Required]
        [MaxLength(length: 30)]
        public string Processor { get; set; }

        [Required]
        [MaxLength(length: 15)]
        public string Memory { get; set; }

        [Required]
        [MaxLength(length: 30)]
        public string Graphics { get; set; }

        [MaxLength(length: 20)]
        public string DirectX { get; set; }

        [MaxLength(length: 50)]
        public string Network { get; set; }

        [Required]
        [MaxLength(length: 50)]
        public string Storage { get; set; }

        [Required]
        [MaxLength(length: 100)]
        public string AdditionalNotes { get; set; }


        [ForeignKey("SystemRequirementsType")]
        public int SystemRequirementsTypeId { get; set; }
        public virtual SystemRequirementsType SystemRequirementsType { get; set; }

        public int GameId { get; set; }
        public virtual Game Game{ get; set; }
    }
}
