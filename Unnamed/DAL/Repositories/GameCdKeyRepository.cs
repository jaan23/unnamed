﻿using Domain.Tables;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class GameCdKeyRepository : Repository<GameCdKey>, IGameCdKeyRepository
    {
        public GameCdKeyRepository(IAppDbContext dbContext) : base(dbContext)
        {
        }

        public GameCdKey GetByCdKey(string key)
        {
            return DbSet.FirstOrDefault(a => a.CDKey.ToUpper() == key.ToUpper());
        }
    }
}
