﻿using Domain.Identity;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class RoleIntRepository : RoleRepository<int, RoleInt, UserInt, UserClaimInt, UserLoginInt, UserRoleInt>,
        IRoleIntRepository
    {
        public RoleIntRepository(IAppDbContext dbContext) : base(dbContext)
        {
        }
    }

    public class RoleRepository : RoleRepository<string, Role, User, UserClaim, UserLogin, UserRole>, IRoleRepository
    {
        public RoleRepository(IAppDbContext dbContext) : base(dbContext)
        {
        }
    }

    public class RoleRepository<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole> : Repository<TRole>,
        IRoleRepository<TKey, TRole>
        where TKey : IEquatable<TKey>
        where TRole : Role<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole>
        where TUser : User<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole>
        where TUserClaim : UserClaim<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole>
        where TUserLogin : UserLogin<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole>
        where TUserRole : UserRole<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole>
    {
        public RoleRepository(IAppDbContext dbContext) : base(dbContext)
        {
        }

        public TRole GetByRoleName(string roleName)
        {
            return DbSet.FirstOrDefault(a => a.Name.ToUpper() == roleName.ToUpper());
        }

        public List<TRole> GetRolesForUser(TKey userId)
        {
            
            return (from role in DbSet from user in role.Users.Where(a => a.UserId.Equals(userId)) select role).ToList();
        }
    }
}