﻿using DAL.Repositories;
using Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Helpers
{
    public class RepositoryFactories : IDisposable
    {
        private readonly NLog.ILogger _logger;
        private readonly string _instanceId = Guid.NewGuid().ToString();

        private readonly IDictionary<Type, Func<IAppDbContext, object>> _repositoryFactories;

        public RepositoryFactories(ILogger logger)
        {
            _logger = logger;
            _logger.Debug("InstanceId: " + _instanceId);

            _repositoryFactories = GetCustomFactories();
        }

        //this ctor is for testing only, you can give here an arbitrary list of repos
        public RepositoryFactories(IDictionary<Type, Func<IAppDbContext, object>> factories, ILogger logger)
        {
            _logger = logger;
            _repositoryFactories = factories;

            _logger.Debug("InstanceId: " + _instanceId);
        }

        //special repos with custom interfaces are registered here
        private static IDictionary<Type, Func<IAppDbContext, object>> GetCustomFactories()
        {
            return new Dictionary<Type, Func<IAppDbContext, object>>
            {
            
                {typeof (IUserIntRepository), dbContext => new UserIntRepository(dbContext)},
                {typeof (IUserRoleIntRepository), dbContext => new UserRoleIntRepository(dbContext)},
                {typeof (IUserClaimIntRepository), dbContext => new UserClaimIntRepository(dbContext)},
                {typeof (IUserLoginIntRepository), dbContext => new UserLoginIntRepository(dbContext)},
                {typeof (IRoleIntRepository), dbContext => new RoleIntRepository(dbContext)},
                {typeof (IGameRepository), dbContext => new GameRepository(dbContext)},
                {typeof(IGameGenreRepository), dbContext => new GameGenreRepository(dbContext) },
                {typeof(IGenreRepository), dbContext => new GenreRepository(dbContext) },
                {typeof(IGameCdKeyRepository), dbContext=> new GameCdKeyRepository(dbContext) },
                {typeof(IOrderRepository), dbContext=> new OrderRepository(dbContext) },
                {typeof(IFeedbackRepository), dbContext => new FeedbackRepository(dbContext) }
                
            };
        }

        public Func<IAppDbContext, object> GetRepositoryFactory<T>()
        {
            Func<IAppDbContext, object> factory;
            _repositoryFactories.TryGetValue(typeof(T), out factory);
            return factory;
        }

        public Func<IAppDbContext, object> GetRepositoryFactoryForEntityType<T>() where T : class
        {
            // if we already have this repository in list, return it
            // if not, create new instance of EFRepository
            return GetRepositoryFactory<T>() ?? DefaultEntityRepositoryFactory<T>();
        }

        protected virtual Func<IAppDbContext, object> DefaultEntityRepositoryFactory<T>() where T : class
        {
            // create new instance of EFRepository<T>
            return dbContext => new Repository<T>(dbContext);
        }

        public void Dispose()
        {
            _logger.Debug("InstanceId: " + _instanceId);
        }
    }
}
