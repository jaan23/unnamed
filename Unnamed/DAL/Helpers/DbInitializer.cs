﻿using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Identity;

namespace DAL.Helpers
{
    public class DbInitializer : DropCreateDatabaseAlways<AppDbContext>
    {
        protected override void Seed(AppDbContext context)
        {
            var genre = new Genre()
            {
                Name = "Action"
            };

            var genre1 = new Genre()
            {
                Name = "Casual"
            };

            var genre2 = new Genre()
            {
                Name = "RPG"
            };

            var g1 = new Game()
            {
                Name = "Grand Theft Auto V",
                Year = 2015,
                FrontCoverUrl = "http://cdn.downdetector.com/static/uploads/c/300/76ed8/gta-v-logo-huge-transback1.png"
            };

            var g2 = new Game()
            {
                Name = "Sniper Ghost Warrior 3",
                Year = 2017,
                FrontCoverUrl = "https://static.gamespot.com/uploads/scale_medium/1197/11970954/2309211-2224534-sniperbox.png"
            };
            var g3 = new Game()
            {
                Name = "Vanquish",
                Year = 2017,
                FrontCoverUrl = "http://4.bp.blogspot.com/-bJsEvG32e_c/T9-BDRaC4JI/AAAAAAAACo4/TyUUesE3VK4/s1600/vanquish+cover.jpg"
            };
            var g4 = new Game()
            {
                Name = "STAR WARS™ Jedi Knight II - Jedi Outcast™",
                Year = 2003,
                FrontCoverUrl = "http://cdn.akamai.steamstatic.com/steam/apps/6030/header.jpg?t=1489458409"
            };
            var g5 = new Game()
            {
                Name = "The Surge",
                Year = 2017,
                FrontCoverUrl = "http://cdn.akamai.steamstatic.com/steam/apps/378540/header.jpg?t=1495189190"
            };
            var g6 = new Game()
            {
                Name = "Prototype™",
                Year = 2009,
                FrontCoverUrl = "https://upload.wikimedia.org/wikipedia/en/b/b2/PROTOTYPE.png"
            };
            var g7 = new Game()
            {
                Name = "Dark Souls Prepare to die",
                Year = 2010,
                FrontCoverUrl = "https://vignette3.wikia.nocookie.net/darksouls/images/b/b9/PTDE_Box_Art.jpg/revision/latest?cb=20121211033707"
            };


            var ggenre = new GameGenre()
            {
                Game = g2,
                Genre = genre
            };

            var ggenre2 = new GameGenre()
            {
                Game = g1,
                Genre = genre1
            };
            var ggenre3 = new GameGenre()
            {
                Game = g3,
                Genre = genre2
            };




            var i1 = new Illustration() {
                Game = g1,
                IllustrationLink = "https://www.mmoga.com/images/screenshots/_p/1015972/b0907a715701698e4b11e31cc2e8d69d_dark-souls-prepare-to-die-edition.jpg"
            };

            var i2 = new Illustration()
            {
                Game = g1,
                IllustrationLink = "https://static.gamespot.com/uploads/scale_super/mig/8/5/7/4/2008574-667257_20120910_007.jpg"
            };

            var i3 = new Illustration()
            { 
                Game = g1,
                IllustrationLink = "https://www.mmoga.com/images/screenshots/_p/1015972/b0907a715701698e4b11e31cc2e8d69d_dark-souls-prepare-to-die-edition.jpg"
            };

            var i4 = new Illustration()
            {
                Game = g1,
                IllustrationLink = "https://www.mmoga.com/images/screenshots/_p/1015972/b0907a715701698e4b11e31cc2e8d69d_dark-souls-prepare-to-die-edition.jpg"
            };

            List<Illustration> ils = new List<Illustration>();
            ils.Add(i1);
            ils.Add(i2);
            ils.Add(i3);
            ils.Add(i4);

            var it = new IllustrationType()
            {
                Name = "image",
                Illustrations = ils
            };


            var sr = new SystemRequirements
            {
                Os = "Win 10",
                Processor = "Intel i5",
                Memory = "4GB",
                Graphics = "Intel gtx 450",
                DirectX = "DirectX 9.0",
                Storage = "35GB",
                AdditionalNotes = "Git gud",
                Game = g1
            };

            var sr2 = new SystemRequirements
            {
                Os = "Win 10",
                Processor = "Intel i5",
                Memory = "4GB",
                Graphics = "Intel gtx 450",
                DirectX = "DirectX 9.0",
                Storage = "35GB",
                AdditionalNotes = "Git gud",
                Game = g1
            };

            List<SystemRequirements> listsr = new List<SystemRequirements>();
            listsr.Add(sr);

            List<SystemRequirements> listsr2 = new List<SystemRequirements>();
            listsr2.Add(sr2);
            var srt = new SystemRequirementsType
            {
                Name = "Minimum",
                SystemRequirements = listsr
            };

            var srt2 = new SystemRequirementsType
            {
                Name = "Reccomended",
                SystemRequirements = listsr2
            };

            context.GameGenres.Add(ggenre);
            context.GameGenres.Add(ggenre2);
            context.GameGenres.Add(ggenre3);

            context.Games.Add(g1);
            context.Games.Add(g2);
            context.Games.Add(g3);
            context.Games.Add(g4);
            context.Games.Add(g5);
            context.Games.Add(g6);
            context.Games.Add(g7);
            context.IllustrationsType.Add(it);
            context.SystemRequirementsType.Add(srt);
            context.SystemRequirementsType.Add(srt2);
            context.SaveChanges();
        }
    }
}
