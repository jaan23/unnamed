﻿using DAL.EFConfiguration;
using DAL.Helpers;
using Domain.Identity;
using Domain.Tables;
using Interfaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        [Inject]
        public AppDbContext()
            : base("name=DataBaseConnectionStr")
        {


            Database.SetInitializer(new DbInitializer());

            // no proxy objects!!!!
            //  Configuration.LazyLoadingEnabled = false;
        }




        // Identity tables, PK - int
        public IDbSet<RoleInt> RolesInt { get; set; }
        public IDbSet<UserClaimInt> UserClaimsInt { get; set; }
        public IDbSet<UserLoginInt> UserLoginsInt { get; set; }
        public IDbSet<UserInt> UsersInt { get; set; }
        public IDbSet<UserRoleInt> UserRolesInt { get; set; }
        public IDbSet<Game> Games { get; set; }
        public IDbSet<Genre> Genres { get; set; }
        public IDbSet<GameGenre> GameGenres { get; set; }
        public IDbSet<Illustration> Illustrations { get; set; }
        public IDbSet<IllustrationType> IllustrationsType { get; set; }
        public IDbSet<SystemRequirements> SystemRequirements { get; set; }
        public IDbSet<SystemRequirementsType> SystemRequirementsType { get; set; }
        public IDbSet<GameCdKey> GameCdKeys { get; set; }
        public IDbSet<Favourite> Favourites { get; set; }
        public IDbSet<Order> Orders { get; set; }
        public IDbSet<GameCdKeyInOrder> GameCdKeysInOrders { get; set; }
        public IDbSet<Feedback> Feedbacks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);

            // remove tablename pluralizing
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            // remove cascade delete
            //modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            // Identity, PK - string 
            //modelBuilder.Configurations.Add(new RoleMap());
            //modelBuilder.Configurations.Add(new UserClaimMap());
            //modelBuilder.Configurations.Add(new UserLoginMap());
            //modelBuilder.Configurations.Add(new UserMap());
            //modelBuilder.Configurations.Add(new UserRoleMap());

            // Identity, PK - int 
            modelBuilder.Configurations.Add(new RoleIntMap());
            modelBuilder.Configurations.Add(new UserClaimIntMap());
            modelBuilder.Configurations.Add(new UserLoginIntMap());
            modelBuilder.Configurations.Add(new UserIntMap());
            modelBuilder.Configurations.Add(new UserRoleIntMap());

            Precision.ConfigureModelBuilder(modelBuilder);

            // convert all datetime and datetime? properties to datetime2 in ms sql
            // ms sql datetime has min value of 1753-01-01 00:00:00.000
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            // use Date type in ms sql, where [DataType(DataType.Date)] attribute is used
            modelBuilder.Properties<DateTime>()
           .Where(x => x.GetCustomAttributes(false).OfType<DataTypeAttribute>()
           .Any(a => a.DataType == DataType.Date))
           .Configure(c => c.HasColumnType("date"));


        }


    }
}
