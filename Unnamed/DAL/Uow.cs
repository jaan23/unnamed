﻿using DAL.Repositories;
using Domain.Identity;
using Domain.Tables;
using Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Uow : IUow, IDisposable
    {
        private readonly NLog.ILogger _logger;
        private readonly string _instanceId = Guid.NewGuid().ToString();

        private IAppDbContext DbContext { get; set; }
        protected IRepositoryProvider RepositoryProvider { get; set; }

        public Uow(IRepositoryProvider repositoryProvider, IAppDbContext dbContext, ILogger logger)
        {
            _logger = logger;
            _logger.Debug("InstanceId: " + _instanceId);

            DbContext = dbContext;

            repositoryProvider.DbContext = dbContext;
            RepositoryProvider = repositoryProvider;
        }

        // UoW main feature - atomic commit at the end of work
        public void Commit()
        {
            ((DbContext)DbContext).SaveChanges();
        }

        public void RefreshAllEntities()
        {
            foreach (var entity in ((DbContext)DbContext).ChangeTracker.Entries())
            {
                entity.Reload();
            }
        }

    
        public IUserIntRepository UsersInt => GetRepo<IUserIntRepository>();
        public IUserRoleIntRepository UserRolesInt => GetRepo<IUserRoleIntRepository>();
        public IRoleIntRepository RolesInt => GetRepo<IRoleIntRepository>();
        public IUserClaimIntRepository UserClaimsInt => GetRepo<IUserClaimIntRepository>();
        public IUserLoginIntRepository UserLoginsInt => GetRepo<IUserLoginIntRepository>();

        public IGameRepository Game => GetRepo<IGameRepository>();
        public IGameGenreRepository GameGenre => GetRepo<IGameGenreRepository>();
        public IGenreRepository Genre => GetRepo<IGenreRepository>();
        public IGameCdKeyRepository GameCdKey =>GetRepo<IGameCdKeyRepository>();
        public IFavouriteRepository Favourite => GetRepo<IFavouriteRepository>();
        public IOrderRepository Order => GetRepo<IOrderRepository>();
        public IFeedbackRepository Feedback => GetRepo<IFeedbackRepository>();
        public ISystemRequirements SystemRequirements => GetRepo<ISystemRequirements>();


        // calling standard EF repo provider
        private IRepository<T> GetStandardRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }

        // calling custom repo provier
        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }

        // try to find repository
        public T GetRepository<T>() where T : class
        {
            var res = GetRepo<T>() ?? GetStandardRepo<T>() as T;
            if (res == null)
            {
                throw new NotImplementedException("No repository for type, " + typeof(T).FullName);
            }
            return res;
        }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _logger.Debug("InstanceId: " + _instanceId + " Disposing:" + disposing);
        }

        #endregion
    }
}
